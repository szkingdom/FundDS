package com.szkingdom.foundds.net;


import com.szkingdom.foundds.entity.BodyResponse;
import com.szkingdom.foundds.entity.News;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import rx.Observable;

/**
 * @author gerry
 */
public interface ApiServer {
    @Headers("apikey:{apikey}")
    @GET("songshuxiansheng/news/news")
    Observable<BodyResponse<ArrayList<News>>> getData(@Header("apikey") String apikey);
}
