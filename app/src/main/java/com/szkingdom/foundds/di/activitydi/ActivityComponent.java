package com.szkingdom.foundds.di.activitydi;


import com.szkingdom.foundds.activity.BaseActivity;

import dagger.Component;

/**
 * @author gerry
 */
@Component(dependencies = ActivityModule.class)
public interface ActivityComponent {
    void inject(BaseActivity baseActivity);
}
