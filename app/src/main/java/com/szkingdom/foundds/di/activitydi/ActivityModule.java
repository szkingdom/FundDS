package com.szkingdom.foundds.di.activitydi;


import com.szkingdom.foundds.net.ApiServer;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author gerry
 */
@Module
public class ActivityModule {
        @Provides
        ApiServer provideRetrofitClient(){
                OkHttpClient okHttpClient = new OkHttpClient();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://apis.baidu.com/")
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                return retrofit.create(ApiServer.class);
        }
}
