package com.szkingdom.foundds.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.szkingdom.foundds.di.activitydi.ActivityModule;
import com.szkingdom.foundds.di.activitydi.DaggerActivityComponent;
import com.szkingdom.foundds.net.ApiServer;

import javax.inject.Inject;

/**
 * @author gerry
 */
public class BaseActivity extends AppCompatActivity {
    @Inject
    ApiServer apiServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder().activityModule(new ActivityModule()).build().inject(this);
    }
}
