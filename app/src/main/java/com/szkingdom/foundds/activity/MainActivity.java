package com.szkingdom.foundds.activity;


import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.szkingdom.foundds.R;
import com.szkingdom.foundds.entity.BodyResponse;
import com.szkingdom.foundds.entity.News;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends BaseActivity {

    @BindView(R.id.iv_main)
    ImageView ivMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        showPic();
    }

     private void showPic() {
        apiServer.getData("50410d89d6db06c2938f59003387626d")
                .flatMap(new Func1<BodyResponse<ArrayList<News>>, Observable<ArrayList<News>>>() {
                    @Override
                    public Observable<ArrayList<News>> call(BodyResponse<ArrayList<News>> arrayListBodyResponse) {
                        Observable<ArrayList<News>> just = Observable.just(arrayListBodyResponse.getRetData());
                        return just;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<News>>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(ArrayList<News> arrayListBodyResponse) {
                        for (News news :
                                arrayListBodyResponse) {
                            if (!TextUtils.isEmpty(news.getImage_url())) {
                                Glide.with(MainActivity.this).load(news.getImage_url()).asBitmap().into(ivMain);
                                break;
                            }
                        }
                    }
                });
    }
}
