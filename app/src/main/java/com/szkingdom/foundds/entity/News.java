package com.szkingdom.foundds.entity;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author gerry
 */
public class News {

    /**
     * title : 身孕九个月了，还敢这样跳肚皮舞
     * url : http://toutiao.com/group/6222856594671436033/
     * abstract : 52肚皮舞网：看着看着，真怕孩子突然就掉出来了……
     * image_url : http://p1.pstatp.com/list/9849/8223744749
     */

    private String title;
    private String url;
    @SerializedName("abstract")
    private String abstractX;
    private String image_url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAbstractX() {
        return abstractX;
    }

    public void setAbstractX(String abstractX) {
        this.abstractX = abstractX;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }
}
